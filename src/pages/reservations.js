import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Card, CardContent, CardActions, Modal, Box } from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const Reservations = () => {
  const [date, setDate] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [open, setOpen] = useState(false);
  const { list } = useSelector((state) => state.reservations);
  const saveReservation = () => {
    console.log("2");
  };

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const checkInputs = (e) => {
    e.preventDefault();
    // if (name.length == 0 || email.length == 0 || phone.length == 0) {
    //   alert("Popr ingresa la información solicitada");
    // } else {
    //   alert("Su reserva se ah realizado con exito");
    // }
    console.log("hola")
  };
  return (
    <div className="divContainer">
      <div>
        <div className="title">
          <label>Confirmar reservacion</label>
        </div>
        <div className="cardContainer">
          {list.map((item, index) => (
            <div key={index}>
              <Card sx={{ minWidth: 275 }}>
                <CardContent key={item.code}>
                  <h4>Destino:</h4>
                  <h3>{item.city.name}</h3>
                  <h2>${item.city.price}</h2>
                </CardContent>
                <div>
                  <label>Seleccion de horario</label>
                </div>
                <select
                  value={date}
                  onChange={(event) => setDate(event.target.value)}
                >
                  {item.city.schedule.map((item, index) => (
                    <option
                      key={item.index}
                      value={item.date + " " + item.time}
                    >
                      {item.date + " " + item.time}
                    </option>
                  ))}
                </select>

                <Modal open={open} onClose={handleClose}>
                  <Box sx={style}>
                    <h3>Datos personales:</h3>
                    <form onSubmit={() => checkInputs}>
                      <div className="inputs">
                        <label>
                          Nombre:
                          <input
                            type="text"
                            value={name}
                            onChange={(event) => setName(event.target.value)}
                          />
                        </label>
                      </div>
                      <div className="inputs">
                        <label>
                          Correo:
                          <input
                            type="text"
                            value={email}
                            onChange={(event) => setEmail(event.target.value)}
                          />
                        </label>
                      </div>
                      <div className="inputs">
                        <label>
                          Celular:
                          <input
                            type="text"
                            value={phone}
                            onChange={(event) => setPhone(event.target.value)}
                          />
                        </label>
                      </div>
                      <input
                        className="button"
                        type="submit"
                        value="Solicitar reserva"
                      />
                    </form>
                  </Box>
                </Modal>
                <CardActions>
                  <input
                    value="Confirmar reservación"
                    className="button"
                    onClick={handleOpen}
                    type="submit"
                  />
                </CardActions>
              </Card>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Reservations;
