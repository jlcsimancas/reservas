import React, { useEffect, useState } from "react";
import "../App.scss";
import db from "../firebase/firebaseconfig";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import { collection, getDocs } from "firebase/firestore";
import { useDispatch, useSelector } from "react-redux";
import { setCityList } from "../store/slices/citys";
import { setReserveList } from "../store/slices/reservations";

const BookingFly = () => {
  const { list } = useSelector((state) => state.citys);
  const { List: reservations } = useSelector((state) => state.reservations);
  const dispatch = useDispatch();
  const [date, setDate] = useState("");

  useEffect(() => {
    const destinations = async () => {
      const array = [];
      const datos = await getDocs(collection(db, "destinations"));
      datos.forEach((item) => {
        array.push(item.data());
      });
      dispatch(setCityList(array));
    };
    destinations();
  }, []);

  const addToCart = (item) => {
    dispatch(setReserveList(item));
    alert("Destino agregado a su carrito");
    console.log(item);
  };

  return (
    <div className="divContainer">
      <div>
        <label>Seleccione su destino de preferencia</label>
        <div className="cardContainer">
          {list.map((item, index) => (
            <div key={index}>
              <Card sx={{ minWidth: 275 }}>
                <CardContent key={item.code}>
                  <h4>Destino:</h4>
                  <h3>{item.city.name}</h3>
                  <h2>${item.city.price}</h2>
                </CardContent>
                <select
                  value={date}
                  onChange={(event) => setDate(event.target.value)}
                >
                  {item.city.schedule.map((item, index) => (
                    <option
                      key={item.index}
                      value={item.date + " " + item.time}
                    >
                      {item.date + " " + item.time}
                    </option>
                  ))}
                </select>
                <CardActions>
                  <input
                    value="Agregar al carrito"
                    className="button"
                    onClick={() => addToCart([item])}
                    type="submit"
                  />
                </CardActions>
              </Card>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default BookingFly;
