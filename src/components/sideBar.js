import { Link } from "react-router-dom";

const SideBar = () => {
  return (
    <div className="sidebar">
      <ul>
        <li>
          <Link to="/">Inicio</Link>
        </li>
        <li>
          <Link to="/booking">Busqueda de vuelos</Link>
        </li>
        <li>
          <Link to="/reservations">Mis reservaciones</Link>
        </li>
      </ul>
    </div>
  );
};

export default SideBar;
