import React from "react";
import "./App.scss";
import Navigation from "./navigation/navigations";
import { Provider } from "react-redux";
import store from "./store";

const App = () => {
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
};

export default App;
