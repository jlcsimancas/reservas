import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration

const firebaseConfig = {
  apiKey: "AIzaSyCC9L6eXypUBS1ji3Gd1RbbcAJ9NIO6Flo",
  authDomain: "reservas-d2832.firebaseapp.com",
  projectId: "reservas-d2832",
  storageBucket: "reservas-d2832.appspot.com",
  messagingSenderId: "420305590194",
  appId: "1:420305590194:web:d4d2664b9e31ca2c47b6ab",
  measurementId: "G-1RLNMVZQH3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);


export default db;