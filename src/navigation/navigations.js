import { BrowserRouter, Route, Routes } from "react-router-dom";
import "../App.scss";
import NavBar from "../components/navBar";
import SideBar from "../components/sideBar";
import Booking from "../pages/booking";
import Home from "../pages/home";
import Reservations from "../pages/reservations";


const Navigation = () => {
  return (
    <BrowserRouter>
        <NavBar />
        <div className="flex">
          <SideBar />
          <div className="content">
            <Routes>
              <Route path="/" exact={true} element={<Home />} />
              <Route path="/booking" exact={true} element={<Booking />} />
              <Route path="/reservations" exact={true} element={<Reservations/>}/>
            </Routes>
          </div>
        </div>
    </BrowserRouter>
  );
};

export default Navigation;
