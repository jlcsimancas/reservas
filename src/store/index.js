import { configureStore } from "@reduxjs/toolkit";
import citys from "./slices/citys";
import reservations from "./slices/reservations";

export default configureStore({
  reducer: {
    citys,
    reservations
  },
});
