import { createSlice } from "@reduxjs/toolkit";

export const reserveSlice = createSlice({
  name: "reserve",
  initialState: {
    list: [],
  },
  reducers: {
    setReserveList: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const {setReserveList} = reserveSlice.actions;

export default reserveSlice.reducer;
