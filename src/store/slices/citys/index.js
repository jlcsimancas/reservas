import { createSlice } from "@reduxjs/toolkit";

export const citySlice = createSlice({
  name: "citys",
  initialState: {
    list: [],
  },
  reducers: {
    setCityList: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const { setCityList } = citySlice.actions;

export default citySlice.reducer;

